﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ComandosBasicos : MonoBehaviour
{
    public void CargarScena(string NombreEscena)
    {
        SceneManager.LoadSceneAsync(NombreEscena);
    }
}
