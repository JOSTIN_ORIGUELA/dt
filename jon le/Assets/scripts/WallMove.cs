﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMove : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject wall;
    public float speed;
    public float velocidadDelapared;
    void Start()
    {
        velocidadDelapared = wall.GetComponent<WallMove>().speed;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        Autodestruction();
        Invoke("VelocidadWall", 10);//esto no funciona porque los prefabs en el juego no tienenn ni 2 segundos de vida, tal vez poniendoselo al generator se pueda;
    }


    void Autodestruction()
    {
        if (transform.position.z < -170)
        {
            Destroy(this.gameObject);
        }
    }

    void VelocidadWall()
    {
        speed = velocidadDelapared + 20;
    }
}
