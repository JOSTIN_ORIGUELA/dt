﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento2 : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject formas;
    private Rigidbody formas_;
    public float velocidad;
    public int carril = 0;
    void Start()
    {
        //formas_ = formas.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(35, 14.2f, -30), velocidad*Time.deltaTime);

            //formas_.MovePosition(formas_.position + new Vector3(2, 0, 0).normalized*velocidad*Time.deltaTime);
        }


        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-35, 14.2f, -30), velocidad * Time.deltaTime);
            //formas_.MovePosition(formas_.position + new Vector3(-2,0,0).normalized*velocidad*Time.deltaTime);
        }
    }
}
