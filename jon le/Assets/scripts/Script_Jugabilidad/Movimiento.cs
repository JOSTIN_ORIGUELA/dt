﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject GameObjectFormas;
    private Rigidbody RigFormas;
    public float velocidad;
    void Start()
    {
        RigFormas = GameObjectFormas.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            RigFormas.AddForce(new Vector3(velocidad, 0, 0), ForceMode.Impulse);
        }


        if (Input.GetKey(KeyCode.LeftArrow))
        {
            RigFormas.AddForce(new Vector3(-velocidad, 0, 0), ForceMode.Impulse);
        }
    }
}
