﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cambio : MonoBehaviour
{
    public GameObject[] formas;
    private string forma;
    void Start()
    {

        forma = "cubo";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            forma = "cubo";
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            forma = "esfera";
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            forma = "capsula";
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            forma = "pirámide";
        }
        Cambio_forma();

    }


    public void Cambio_forma()
    {
        for (int i = 0; i < formas.Length; i++)
        {
           

            if (formas[i].gameObject.tag == forma)
            {
                formas[i].gameObject.SetActive(true);
            }
            else
            {
                formas[i].gameObject.SetActive(false);
            }
        }
    }
}
