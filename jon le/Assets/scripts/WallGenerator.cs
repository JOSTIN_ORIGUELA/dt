﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : MonoBehaviour
{
    // Start is called before the first frame update
    public float SpawnTime;
    public List<GameObject> WallPref;
    public List<Vector3> posiciones;
    private int random_prefab;
    private int random_posicion_prefab;
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void Spawn()
    {
        random_prefab = Random.Range(0, WallPref.Count);
        random_posicion_prefab = Random.Range(0, 2);
        Instantiate(WallPref[random_prefab], posiciones[random_posicion_prefab], transform.rotation);
        Invoke("Spawn", SpawnTime);
    }
}
